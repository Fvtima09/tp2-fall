package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.concurrent.ExecutionException;

;

import java.util.List;
import java.util.concurrent.Future;


import fr.uavignon.ceri.tp2.data.Book;


import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;


public class BookRepository {

    private MutableLiveData<Book>selectedBook = new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;




    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
        selectedBook.setValue(book);
    }

    public void insertBook(Book newBook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newBook);
        });
        selectedBook.setValue(newBook);;

    }


    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }



    public void getBook(Long id) {

        Future<Book> sBook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(sBook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
