package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

import static android.text.TextUtils.isEmpty;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    private DetailViewModel viewModel;
    private RecyclerAdapter adapter;

    private List<Book> bookList;





    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);


        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
         viewModel.getBook(args.getBookNum());



        textTitle = getView().findViewById(R.id.nameBook);
        textAuthors = getView().findViewById(R.id.editAuthors);
        textYear = getView().findViewById(R.id.editYear);
        textGenres = getView().findViewById(R.id.editGenres);
        textPublisher = getView().findViewById(R.id.editPublisher);


        observerSetup();


        listenerSetup();




        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });



    }

    private void listenerSetup() {
        Button update = getView().findViewById(R.id.buttonUpdate);
        update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String titre = textTitle.getText().toString();
                String auteur = textAuthors.getText().toString();
                String year = textYear.getText().toString();
                String genres = textGenres.getText().toString();
                String publisher = textPublisher.getText().toString();

                if ( isEmpty(titre) || isEmpty(auteur) ){
                    Snackbar.make(view, "Informations incomplètes", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                } else {

                    Book book = new Book(titre,auteur,year,genres,publisher);
                    DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                    long id= args.getBookNum();
                    viewModel.insertOrUpdateBook(book,id);

                    Snackbar.make(view, "Le livre a bien été mis à jour/ajouté", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }

        });
    }



    private void observerSetup() {

        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),selectedBook -> {
            if(selectedBook!=null){

               //textTitle.setText("ID="+selectedBook.getId());
                textTitle.setText(selectedBook.getTitle());
                textAuthors.setText(selectedBook.getAuthors());
                textYear.setText(selectedBook.getYear());
                textGenres.setText(selectedBook.getGenres());
                textPublisher.setText(selectedBook.getPublisher());

            }

        }
            );
    }

}