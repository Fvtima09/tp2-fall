package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel{

    private BookRepository repository;
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<Book> selectedBook;

    public DetailViewModel (Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
        selectedBook = repository.getSelectedBook();
    }

    MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public void insertBook(Book book) {
        repository.insertBook(book);
    }

   public void insertOrUpdateBook(Book book,long id){

        if (id==-1){

            insertBook(book);
        }else {
            book.setId(id);
            updateBook(book);
        }
    }

    public void updateBook(Book book) {
        repository.updateBook(book);
    }


    public void getBook(long id) {
        repository.getBook(id);
    }
    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
}
